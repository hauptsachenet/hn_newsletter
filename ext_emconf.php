<?php

$EM_CONF[$_EXTKEY] = array (
	'title' => 'Newsletter registration',
	'description' => 'Newsletter registration based on formhandler extension',
	'category' => 'plugin',
	'author' => 'Kay Wienöbst',
	'author_email' => 'kay@hauptsache.net',
	'state' => 'alpha',
	'uploadfolder' => false,
	'clearCacheOnLoad' => 1,
	'author_company' => 'hauptsache.net',
	'version' => '1.0.0',
	'constraints' => 
	array (
		'depends' => 
		array (
		),
		'conflicts' => 
		array (
		),
		'suggests' => 
		array (
		)
	),
	'createDirs' => NULL,
	'clearcacheonload' => true
);

?>