<?php
/**
 * Created by PhpStorm.
 * User: kay
 * Date: 16.01.17
 * Time: 16:05
 */

namespace Hn\HnNewsletter\PreProcessor;

use Typoheads\Formhandler\Finisher\Mail;

/**
 * Class ValidateAuthCode
 *
 * validates auth code and sends notification email to admin.
 *
 * @package Hn\HnNewsletter\PreProcessor
 */
class ValidateAuthCode extends Mail
{

    /**
     * Check auth code and send emails. This methods code is 100% copy&paste
     * from \Typoheads\Formhandler\PreProcessor\ValidateAuthCode!!!
     * Only the email sending part has been added.
     *
     * @return array The probably modified GET/POST parameters
     * @throws \Exception
     */
    public function process()
    {
        if (strlen(trim($this->gp['authCode'])) > 0) {

            try {
                $authCode = trim($this->gp['authCode']);
                $table = trim($this->gp['table']);
                if ($this->settings['table']) {
                    $table = $this->utilityFuncs->getSingle($this->settings, 'table');
                }
                $uidField = trim($this->gp['uidField']);
                if ($this->settings['uidField']) {
                    $uidField = $this->utilityFuncs->getSingle($this->settings, 'uidField');
                }
                if (strlen($uidField) === 0) {
                    $uidField = 'uid';
                }
                $uid = trim($this->gp['uid']);

                if (!(strlen($table) > 0 && strlen($uid) > 0)) {
                    $this->utilityFuncs->throwException('validateauthcode_insufficient_params');
                }

                $uid = $GLOBALS['TYPO3_DB']->fullQuoteStr($uid, $table);

                //Check if table is valid
                $existingTables = array_keys($GLOBALS['TYPO3_DB']->admin_get_tables());
                if (!in_array($table, $existingTables)) {
                    $this->utilityFuncs->throwException('validateauthcode_insufficient_params');
                }

                //Check if uidField is valid
                $existingFields = array_keys($GLOBALS['TYPO3_DB']->admin_get_fields($table));
                if (!in_array($uidField, $existingFields)) {
                    $this->utilityFuncs->throwException('validateauthcode_insufficient_params');
                }

                $hiddenField = 'disable';
                if ($this->settings['hiddenField']) {
                    $hiddenField = $this->utilityFuncs->getSingle($this->settings, 'hiddenField');
                } elseif ($TCA[$table]['ctrl']['enablecolumns']['disable']) {
                    $hiddenField = $TCA[$table]['ctrl']['enablecolumns']['disable'];
                }
                $selectFields = '*';
                if ($this->settings['selectFields']) {
                    $selectFields = $this->utilityFuncs->getSingle($this->settings, 'selectFields');
                }
                $hiddenStatusValue = 1;
                if (isset($this->settings['hiddenStatusValue'])) {
                    $hiddenStatusValue = $this->utilityFuncs->getSingle($this->settings, 'hiddenStatusValue');
                }
                $hiddenStatusValue = $GLOBALS['TYPO3_DB']->fullQuoteStr($hiddenStatusValue, $table);
                $enableFieldsWhere = '';
                if (intval($this->utilityFuncs->getSingle($this->settings, 'showDeleted')) !== 1) {
                    $enableFieldsWhere = $this->cObj->enableFields($table, 1);
                }
                $query = $GLOBALS['TYPO3_DB']->SELECTquery($selectFields, $table, $uidField . '=' . $uid . ' AND ' . $hiddenField . '=' . $hiddenStatusValue . $enableFieldsWhere);
                $this->utilityFuncs->debugMessage('sql_request', [$query]);
                $res = $GLOBALS['TYPO3_DB']->sql_query($query);
                if ($GLOBALS['TYPO3_DB']->sql_error()) {
                    $this->utilityFuncs->debugMessage('error', [$GLOBALS['TYPO3_DB']->sql_error()], 3);
                }
                if (!$res || $GLOBALS['TYPO3_DB']->sql_num_rows($res) === 0) {
                    $this->utilityFuncs->throwException('validateauthcode_no_record_found');
                }

                $row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
                $GLOBALS['TYPO3_DB']->sql_free_result($res);
                $this->utilityFuncs->debugMessage('Selected row: ', [], 1, $row);

                $localAuthCode = \TYPO3\CMS\Core\Utility\GeneralUtility::hmac(serialize($row), 'formhandler');

                $this->utilityFuncs->debugMessage('Comparing auth codes: ', [], 1, ['Calculated:' => $localAuthCode, 'Given:' => $authCode]);
                if ($localAuthCode !== $authCode) {
                    $this->utilityFuncs->throwException('validateauthcode_invalid_auth_code');
                }

                // send emails
                $this->sendEmails($row);

                $activeStatusValue = 0;
                if (isset($this->settings['activeStatusValue'])) {
                    $activeStatusValue = $this->utilityFuncs->getSingle($this->settings, 'activeStatusValue');
                }
                $res = $GLOBALS['TYPO3_DB']->exec_UPDATEquery($table, $uidField . '=' . $uid, [$hiddenField => $activeStatusValue]);
                if (!$res) {
                    $this->utilityFuncs->throwException('validateauthcode_update_failed');
                }

                $this->utilityFuncs->doRedirectBasedOnSettings($this->settings, $this->gp);
            } catch (\Exception $e) {
                $redirectPage = $this->utilityFuncs->getSingle($this->settings, 'errorRedirectPage');
                if ($redirectPage) {
                    $this->utilityFuncs->doRedirectBasedOnSettings($this->settings, $this->gp, 'errorRedirectPage');
                } else {
                    throw new \Exception($e->getMessage());
                }
            }
        }
        return $this->gp;
    }

    protected function sendEmails($row)
    {
        // write db data to gp, so that the data can be printed in emails
        foreach ($row as $name => $value) {
            if (!array_key_exists($name, $this->gp)) {
                $this->gp[$name] = $value;
            }
        }

        $this->initMailer('admin');
        $this->sendMail('admin');
    }

}